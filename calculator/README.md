# Text blocks, Switch and var

## Description
The practice of writing code using new java features.

## Details

Work in package 'calculator/src/main/java/mjc/calculator/task'.

<h5>Task 1.</h5>
Update [Calculator](calculator/src/main/java/mjc/structuring/task/Calculator.java) class following the below guides and notes in the comments.
1. Printed answer should have the format:
    - line with the first integer alligned right
    - sign of the math operation, followed by space and second integer
    - line with a prepared delimiter
    - answer with a floating point and two digits after the point
    - update `getStringResult` and `getPrefix` methods to achieve the task
   

Example:<br>
> &nbsp;&nbsp;1000<br>
&#42; 1000<br>
&#95;&#95;&#95;&#95;&#95;<br>
1000000.00

>&nbsp;&nbsp;&nbsp;25<br>
&#42; 100<br>
&#95;&#95;&#95;&#95;&#95;<br>
2500.00

2. `getStringResult` should return lambda with annotation typed arguments validation
    limiting the value for maximum value as 1000 and minimum value as -1000 and using Validation API
    (don't add a validator, just add needed constraints)
    Lambda should perform one of the calculation operation: +, -, ?, *
    For other single sing operand throw UnsupportedOperationException
    For any other char throw Runtime Exception

3. No comments should be left in the code
