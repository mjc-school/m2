package mjc.calculator.task;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CalculatorTest {
    private CalculatorReady calculator;

    @BeforeEach
    public void setUp() {
        calculator = new CalculatorReady();
    }

    @Test
    void plusValidTest() {
        assertEquals(2.0, calculator.getCalculationOperation('+').applyAsDouble(1, 1));
    }

    @Test
    void minusValidTest() {
        assertEquals(0.0, calculator.getCalculationOperation('+').applyAsDouble(-1000, 1000));
    }

    @Test
    void multiplicationValidTest() {
        assertEquals(-1000000.0, calculator.getCalculationOperation('*').applyAsDouble(-1000, 1000));
    }

    @Test
    void divisionValidTest() {
        assertTrue(calculator.getCalculationOperation('/').applyAsDouble(100, 7) - 14.285 < 0.001);
    }

    @Test
    void restOfDivisionExceptionTest() {
        assertThrows(UnsupportedOperationException.class, () ->
            calculator.getCalculationOperation('%')
        );
    }

    @Test
    void andExceptionTest() {
        assertThrows(UnsupportedOperationException.class, () ->
            calculator.getCalculationOperation('&')
        );
    }

    @Test
    void orExceptionTest() {
        assertThrows(UnsupportedOperationException.class, () ->
            calculator.getCalculationOperation('|')
        );
    }

    @Test
    void aCharExceptionTest() {
        assertThrows(RuntimeException.class, () ->
            calculator.getCalculationOperation('a')
        );
    }

    @Test
    void bCharExceptionTest() {
        assertThrows(RuntimeException.class, () ->
            calculator.getCalculationOperation('b')
        );
    }

    @Test
    void xCharExceptionTest() {
        assertThrows(RuntimeException.class, () ->
            calculator.getCalculationOperation('x')
        );
    }

    @Test
    void zCharExceptionTest() {
        assertThrows(RuntimeException.class, () ->
            calculator.getCalculationOperation('z')
        );
    }

    @Test
    void getStringResultMultiplicationTest() {
        assertEquals("   1000\n* -1000\n_____\n-1000000.00", calculator.getStringResult(1000, -1000, '*'));
    }

    @Test
    void getStringResultDivisionTest() {
        assertEquals("100\n/ 7\n_____\n14.29", calculator.getStringResult(100, 7, '/'));
    }

    @Test
    void getStringResultPlusTest() {
        assertEquals("1000\n+ 2\n_____\n1002.00", calculator.getStringResult(1000, 2, '+'));
    }

    @Test
    void getStringResultMinusTest() {
        assertEquals("    1\n- 200\n_____\n-199.00", calculator.getStringResult(1, 200, '-'));
    }
}
