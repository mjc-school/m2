package mjc.calculator.task;

public class Calculator {
    public static final String PRINTED_ANSWER = """
            //fill the block
                 _____
            """;

    public MathCalcOperator getCalculationOperation(char operation) {
        // method should return lambda with arguments validation
        // correspondent to one of the calculation operation: +, -, ?, *
        // for other single sing operand throw UnsupportedOperationException
        // for any other thar throw Runtime Exception
        // argument validation should limit the value for maximum value as 1000 and minimum value as -1000
    }

    public String getStringResult(int a, int b, char c) {
        double result = getCalculationOperation(c).applyAsDouble(a, b);
        //update the below code to have a correct output
        return getPrefix(a, b).concat(PRINTED_ANSWER);
    }

    private String getPrefix(int a, int b) {
        //method should return prefix
        return null;
    }
}
