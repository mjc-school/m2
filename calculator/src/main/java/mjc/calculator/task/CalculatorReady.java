package mjc.calculator.task;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

public class CalculatorReady {
    public static final String PRINTED_ANSWER = """
            %d
            %c %d
            _____
            %s""";

    public MathCalcOperator getCalculationOperation(char operation) {
        return switch (operation) {
            case '+' -> (@Size(min = -1000, max = 1000) var a, @Size(min = -1000, max = 1000) var b) -> a + b;
            case '-' -> (@Min(-1000) @Max(1000) var a, @Min(-1000) @Max(1000) var b) -> a - b;
            case '*' -> (@Size(min = -1000, max = 1000) var a, @Size(min = -1000, max = 1000) var b) -> a * b;
            case '/' -> (@Size(min = -1000, max = 1000) var a, @Size(min = -1000, max = 1000) var b) -> 1.0 * a / b;
            case '%', '&', '|', '^', '~' -> throw new UnsupportedOperationException();
            default -> throw new RuntimeException();
        };
    }

    public String getStringResult(int a, int b, char c) {
        double result = getCalculationOperation(c).applyAsDouble(a, b);
        String formattedResult = String.format("%.2f", result);
        return getPrefix(a, b).concat(PRINTED_ANSWER.formatted(a, c, b, formattedResult));
    }

    private String getPrefix(int a, int b) {
        int aLength = Integer.toString(a).length();
        int bLength = Integer.toString(b).length();

        if (aLength > bLength) {
            return "";
        } else {
            int count = bLength - aLength + 2;
            return " ".repeat(count);
        }
    }
}
