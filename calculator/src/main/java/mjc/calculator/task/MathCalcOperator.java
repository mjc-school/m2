package mjc.calculator.task;

@FunctionalInterface
public interface MathCalcOperator {
    double applyAsDouble(int t, int u);
}
