# Structuring and encapsulation

## Description
The practice of writing records and sealed classes.

## Details

Work in package 'modernjava/src/main/java/mjc/structuring/task'.

Task 1.

Rewrite [Person](modernjava/src/main/java/mjc/structuring/task/Person.java) class as a record.

Task 2.

1. Create interface Thing.
2. Create class Toy, interface Furniture and class Mirror. Make it so that only these entities can be inherited from Thing.
3. Class Toy should be available for further inheritance.
4. Create records Table and Chair. Make it so that only these entities can be inherited from Furniture.
5. Class Mirror shouldn't be available for further inheritance.
