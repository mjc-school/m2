package mjc.structuring.task;

import java.util.Objects;

public class Person {
  private final String name;
  private final int age;

  Person(String name, int age) {
    this.name = name;
    this.age = age;
  }

  Person(String firstName, String lastName, int age) {
    this(firstName + " " + lastName, age);
  }

  String name() {
    return name;
  }

  int age() {
    return age;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Person person = (Person) o;
    return age == person.age && Objects.equals(name, person.name);
  }

  public int hashCode() {
    return Objects.hash(name, age);
  }

  public String toString() {
    return String.format("Person[name=%s, age=%d]", name, age);
  }
}