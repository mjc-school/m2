package mjc.structuring.task;

import static java.nio.file.Files.readAllLines;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.lang.reflect.Modifier;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;


class SealedClassesTest {

  private static Stream<?> getFileLines(Path sourcePath) {
    try {
      return readAllLines(sourcePath, StandardCharsets.UTF_8).stream();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return Collections.emptyList().stream();
  }

  @Test
  public void testHaveAllRequiredElements() throws IOException {
    String result = Files.walk(Paths.get("src/main/java/mjc/structuring/task"))
        .filter(Files::isRegularFile)
        .filter(p -> p.toString().endsWith(".java"))
        .flatMap(SealedClassesTest::getFileLines)
        .map(Object::toString)
        .collect(Collectors.joining("\n"));

    List<String> requiredElements = List.of("class", "interface", "record", "final", "sealed", "non-sealed");
    requiredElements.forEach(el ->
        assertTrue(result.contains(el), String.format("'%s' should be used", el))
    );
  }

  @Test
  public void testThingIsSealed() {
    Optional<Class> thingInterface = getThingInterface();
    boolean isSealed = thingInterface.map(Class::isSealed).orElse(false);
    assertTrue(isSealed, "'Thing' interface should have restriction for inheritance.");
  }

  @Test
  public void testThingPermitsSpecifiedInheritors() {
    Optional<Class> thingInterface = getThingInterface();
    List<String> actual = thingInterface.isPresent()
        ? Arrays.stream(thingInterface.get().getPermittedSubclasses())
        .map(Class::getSimpleName)
        .collect(Collectors.toList())
        : Collections.emptyList();
    List<String> expected = List.of("Toy", "Furniture", "Mirror");
    assertEquals(expected, actual, "'Thing' interface should permits the specified inheritors.");
  }

  @Test
  public void testToyIsAvailableForFurtherInheritance() {
    Class toyClass = new Toy().getClass();
    String message = "'Toy' class should be available for further inheritance.";
    assertFalse(toyClass.isSealed(), message);
    assertFalse(Modifier.isFinal(toyClass.getModifiers()), message);
  }

  @Test
  public void testFurnitureIsSealed() {
    Optional<Class> thingInterface = getFurnitureInterface();
    boolean isSealed = thingInterface.map(Class::isSealed).orElse(false);
    assertTrue(isSealed, "'Furniture' interface should have restriction for inheritance.");
  }

  @Test
  public void testFurniturePermitsSpecifiedInheritors() {
    Optional<Class> thingInterface = getFurnitureInterface();
    List<String> actual = thingInterface.isPresent()
        ? Arrays.stream(thingInterface.get().getPermittedSubclasses())
        .map(Class::getSimpleName)
        .collect(Collectors.toList())
        : Collections.emptyList();
    List<String> expected = List.of("Table", "Chair");
    assertEquals(expected, actual, "'Furniture' interface should permits the specified inheritors.");
  }

  @Test
  public void testTableAndChairAreRecords() {
    Furniture table = new Table();
    Furniture chair = new Chair();
    assertTrue(table.getClass().isRecord(), "'Table' should be a record.");
    assertTrue(chair.getClass().isRecord(), "'Chair' should be a record.");
  }

  @Test
  public void testMirrorIsNotAvailableForFurtherInheritance() {
    Class toyClass = new Mirror().getClass();
    assertTrue(Modifier.isFinal(toyClass.getModifiers()), "'Mirror' class should be available for further inheritance.");
  }

  private Optional<Class> getThingInterface() {
    Thing mirror = new Mirror();
    Class[] interfaces = mirror.getClass().getInterfaces();
    return Arrays.stream(interfaces)
        .filter(i -> i.getSimpleName().equals("Thing"))
        .findFirst();
  }

  private Optional<Class> getFurnitureInterface() {
    Thing table = new Table();
    Class[] interfaces = table.getClass().getInterfaces();
    return Arrays.stream(interfaces)
        .filter(i -> i.getSimpleName().equals("Furniture"))
        .findFirst();
  }
}