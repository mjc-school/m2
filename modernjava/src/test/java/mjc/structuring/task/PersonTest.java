package mjc.structuring.task;

import static java.nio.file.Files.readAllLines;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.jupiter.api.Test;

class PersonTest {

  @Test
  public void testNoPersonClass() throws IOException {
    Files.walk(Paths.get("src/main/java/mjc/structuring/task"))
        .filter(Files::isRegularFile)
        .filter(p -> p.toString().endsWith("Person.java"))
        .forEach(sourcePath -> {
          String result;
          try {
            result = String.join("\n", readAllLines(sourcePath, StandardCharsets.UTF_8));
          } catch (IOException e) {
            throw new IllegalStateException(e);
          }
          assertFalse(result.contains("class"), "Person should be a record.");
        });
  }

  @Test
  public void testPersonIsRecord() {
    Person person = new Person("John", 25);
    String actual = person.getClass().getSuperclass().getName();
    assertEquals("java.lang.Record", actual, "Person should be a record.");
  }

  @Test
  public void testDefaultToString() {
    testDefaultToStringMethod(new Person("Kate", 17));
    testDefaultToStringMethod(new Person("Anna", 35));
  }

  public void testDefaultToStringMethod(Person person) {
    String expected = String.format("Person[name=%s, age=%d]", person.name(), person.age());
    String actual = person.toString();
    String message = String.format("Your program must print \"%s\" but printed \"%s\" instead.", expected, actual);
    assertEquals(expected, actual, message);
  }

  @Test
  public void testPersonHasSeveralConstructors() {
    Person person1 = new Person("John", "Smith", 25);
    Person person2 = new Person("John Smith", 25);
    assertEquals(person2, person1);
  }
}